Vue.directive("touch", {
  bind: function(el, binding) {
    if (typeof binding.value === "function") {
      const mc = new Hammer(el);
      mc.get("pan").set({ direction: Hammer.DIRECTION_ALL });
      mc.on("pan", binding.value);
    }
  }
});

const app = new Vue({
  el: '#app',
  data: {
    pannel: 0,
    extra: false,
    content: '',
    vidId: ''
  },
  methods: {
    onPan(event) {
      const deltaX = event.deltaX; // moved distance on x-axis
      const deltaY = event.deltaY; // moved distance on y-axis
      const isFinal = event.isFinal; // pan released
      const direction = event.direction; // 0 = none, 2 = left, 4 = right, 8 = up, 16 = down,

      if(isFinal && direction == 2) {
        this.pannel = this.pannel >= 5 ? 0 : this.pannel+1;
      } else if(isFinal && direction == 4) {
        this.pannel = this.pannel <= 1 ? 0 : this.pannel-1;
      }
    }
  }
})
